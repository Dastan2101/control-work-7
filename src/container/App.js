import React, {Component} from 'react';
import './App.css';
import hamburger from '../components/assets/hamburger.jpg';
import cheeseburger from '../components/assets/cheeseburger.jpg';
import fries from '../components/assets/fries.jpg';
import tea from '../components/assets/tea.jpg';
import cola from '../components/assets/cola.png';
import coffee from '../components/assets/coffee.jpg';
import CreateComponents from '../components/CreateComponents/CreateComponents';
import OrderList from '../components/OrderList/OrderList';

class App extends Component {

    state = {
        ingredients: [
            {name: 'Hamburger', count: 0, price: 80, image: hamburger, show: false},
            {name: 'Coffee', count: 0, price: 70, image: coffee, show: false},
            {name: 'Tea', count: 0, price: 50, image: tea, show: false},
            {name: 'Cola', count: 0, price: 40, image: cola, show: false},
            {name: 'Cheeseburger', count: 0, price: 30, image: cheeseburger, show: false},
            {name: 'Fries', count: 0, price: 45, image: fries, show: false},
        ],
        total: 0,
        text: 'Order is empty, please add some ingredients',
    };

    addIngredient = (name) => {
        const copyIngredients = [...this.state.ingredients];

        let total = this.state.total;

        for (let i = 0; i < copyIngredients.length; i++) {
            if (copyIngredients[i].name === name) {
                copyIngredients[i].count++;
                total += copyIngredients[i].price;
                copyIngredients[i].show = true;
            }
        }

        this.setState({ingredients: copyIngredients, total: total})

    };

    DeleteIngredient = (name) => {
        const copyIngredients = [...this.state.ingredients];

        let total = this.state.total;


        for (let i = 0; i < copyIngredients.length; i++) {
            if (copyIngredients[i].name === name) {
                copyIngredients[i].count--;
                total -= copyIngredients[i].price;
                if (copyIngredients[i].count === 0) {
                    copyIngredients[i].show = false;

                }
            }

        }

        this.setState({ingredients: copyIngredients, total: total})

    };


    render() {
        return (
            <div className="App">
                <div className="main-div">
                    <div className="add-items">
                        <CreateComponents
                            array={this.state.ingredients}
                            addIngredient={(name) => this.addIngredient(name)}
                            DeleteIngredient={this.DeleteIngredient}
                        />

                    </div>
                    <div className="add-orders">
                        <OrderList orders={this.state.ingredients} total={this.state.total} text={this.state.text}/>

                    </div>
                </div>
            </div>
        );
    }
}

export default App;
