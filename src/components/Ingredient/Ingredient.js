import React, {Fragment} from 'react';
import './Ingredient.css'

const Ingredient = (props) => {
    const Ingredients = () => {
        const ingredientArray = [];
        for (let i = 0; i < props.count; i++) {
            let object = {
                name: props.name,
                count: props.count,
                price: props.price * props.count,
            };

            ingredientArray.splice(object, 1);
            ingredientArray.push(object);
        }

        return ingredientArray
    };


    return (
        <Fragment>
            {Ingredients().map((index, key) => {
                return (
                    <div key={key} className="order-div">
                        <h4>{index.name} </h4>
                        <span>x:{index.count}</span>
                        <span>{index.price} KGS</span>
                    </div>
                )
            })}
        </Fragment>
    )
};

export default Ingredient;