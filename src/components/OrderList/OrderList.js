import React from 'react';
import './OrderList.css';
import Ingredient from '../Ingredient/Ingredient'

const OrderList = (props) => {
    return (
        <div className="orders-div">
            {props.orders.map((order, key) => {
                return (
                    <Ingredient key={key} name={order.name} count={order.count} price={order.price}
                                DeleteIngredient={order.name}/>

                )
            })}
            <h5 className="text">{props.total === 0 ? props.text : null}</h5>

            <h3 className="total-price-title">Total price: {props.total}</h3>

        </div>
    )
};

export default OrderList;