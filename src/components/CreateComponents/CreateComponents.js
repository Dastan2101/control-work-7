import React from 'react';
import './Components.css'

const CreateComponents = props => {
    return (
        props.array.map((object, key) => {
            return (
                <div className="add-ingredient" key={key}>
                    <div className="ingredient-block" onClick={() => props.addIngredient(object.name)}>
                        <img src={object.image} alt="#"/>
                        <h5 className="title-name">{object.name}</h5>
                        <h6 className="title-price">Price: {object.price}</h6>
                    </div>
                    {object.show ? <button className="delete-btn" onClick={() => props.DeleteIngredient(object.name)}> </button> : null}
                </div>

            )
        })
    )
};

export default CreateComponents;